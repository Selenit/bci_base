package com.bci.genericos.aliados;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.bci.common.Common;

public class PagosMasivos extends Common {
	
	/******************************************INPUT******************************************/
	public By inputRut =By.xpath("//input[@placeholder='RUT']");
	public By inputContraseña =By.xpath("//input[@placeholder='Clave']");
	public By inputRutBeneficiario =By.xpath("(//input[@id='busqueda-pagos-input-rut'])[1]");
	public By inputFolio =By.xpath("//input[@id='busqueda-pagos-input-folio']");
	
	/******************************************BOTON******************************************/
	public By btnIngresar =By.xpath("//button[contains(text(),'INGRESAR')]");
	
	public By btnPagosyTransferencias =By.xpath("//button[contains(text(),'INGRESAR')]");
	public By btnConsultarPagosyTransferencias =By.xpath("//div[contains(text(),'Consultar pagos y transferencias')]");
	public By btnTodasLasEmpresas =By.xpath("(//div[@class='mat-select-arrow'])[1]");
	
	
	/******************************************INPUT******************************************/
	
	
	
	/******************************************OPCION******************************************/
	public By optCalendario =By.xpath("//button[@aria-label='Choose month and year']");
	public By optFechaDesde =By.xpath("(//input[@placeholder='Fecha de pago desde'])[1]");
	public By optFechaHasta =By.xpath("(//input[@placeholder='Fecha de pago hasta'])[1]");
	public By optAño =By.xpath("//td[@aria-label='2019']");		
	public By optMes =By.xpath("//div[text()='JUL.']");
	public By optDia =By.xpath("//div[text()='9']");
	public By optPagosMasivos =By.xpath("//span[contains(text(),'Pagos Masivos')]");
	public By optDividendos =By.xpath("//span[contains(text()'Dividendos')]");
	
	
	/******************************************DropDown******************************************/
	public By dDwnProductoPago =By.xpath("//span[contains(text(),'Producto de pago')]");
	public By dDwnTipoOperacion =By.xpath("//mat-select[@id='busqueda-pagos-tipo-operacion']");
	
	
	/******************************************METODOS******************************************/
	
	public PagosMasivos(WebDriver driver) {
		super(driver);
	
	}
	
	public void ingresarLogin() {
		sendKeyElemento(inputRut, "10265168-5", "Input Buscar");
		sendKeyElemento(inputContraseña, "webdesa1", "Input Contraseña");
		clicElemento(btnIngresar,"Boton Ingresar");
	}
	
	public void ingresarModulo() {
		clicElemento(btnPagosyTransferencias,"Boton Ingresar");
		clicElemento(btnConsultarPagosyTransferencias,"Boton Ingresar");
	}
}
