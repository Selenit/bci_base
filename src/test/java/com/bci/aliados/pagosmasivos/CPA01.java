package com.bci.aliados.pagosmasivos;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import com.bci.common.Common;
import com.bci.genericos.aliados.PagosMasivos;

public class CPA01 {

	WebDriver driver;
	private static String baseUrl;
	@Test
	public void test() throws Exception {
		driver = new ChromeDriver();
		PagosMasivos CPA01_Logic  = new PagosMasivos(driver);
		Common common  = new Common(driver);
		driver.manage().window().maximize();
		
		try {
			baseUrl = "https://front-crt01.bci.cl/nuevaWeb/fe-empresasbase-re-v1-36-2/login";
			driver.get(baseUrl);
			
			/*================ Paso 1 ===========================*/
			CPA01_Logic.ingresarLogin();
			
			
		} catch (Exception e) {
			common.capturaPantalla();
			common.logger("Ha fallado el test");
		}finally {
			driver.close();
			driver.quit();
		}
		
	 }
    }


